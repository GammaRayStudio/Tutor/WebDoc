VSCode - Web IDE
======
`Visual Studio Code - Web Integrated Development Environment`

+ 官網 : <https://code.visualstudio.com/>
+ 下載 : <https://code.visualstudio.com/download>

<br>

### Plugin
+ Live Server `即時預覽`
+ Bracket Pair Colorizer `括號樣式`
+ Path Intellisense `補全路徑`
+ Better Comments `註解樣式`
+ Prettier — Code formatter `自動排版`
+ HTML CSS Support `程式補全`
+ intelliSense for CSS class name in HTML `CSS 補全`
+ Colonize `結尾分號`
+ Material Icon Theme `圖示樣式`

<br>

### KeyMap 
+ Mac : <https://code.visualstudio.com/shortcuts/keyboard-shortcuts-macos.pdf>
+ Win : <https://code.visualstudio.com/shortcuts/keyboard-shortcuts-windows.pdf>

**Mac**
+ Explorer : cmd + shift + E
+ Terminal : ctrl + `
+ Format : opt + shift + F

**Win**
+ Explorer : ctrl + shift + E
+ Terminal : ctrl + `
+ Format : alt + shift + F

<br>

Reference
------
### Visual Studio Code開發網站入門教學
<https://medium.com/@success85911/visual-studio-code%E9%96%8B%E7%99%BC%E7%B6%B2%E7%AB%99%E5%85%A5%E9%96%80%E6%95%99%E5%AD%B8-7514ea9299bf>








