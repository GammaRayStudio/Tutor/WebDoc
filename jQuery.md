jQuery 入門指南
======


目錄
------
+ Javascript
+ jQuery - Basic
  + 比較 : jQuery vs Javascript
  + jQuery 使用方法 `官網範例`
  + jQuery 操作元素 `F12、Console`
+ jQuery - Sample
  + Parameter `變數`
  + Function `函數`
  + Selectors `選擇器`
  + Attributes `屬性`
  + CSS Style`樣式`
+ jQuery Ajax
+ jQuery UI
  + Sample 01 - Template
  + Sample 02 - Handle
+ jQuery Plugin
  + Moment
  + DataTable
+ Architecture

<br>

Javascript
------
`Html 與 Javascript 關係 : 用於網頁的互動功能`
+ 同一檔案 : script 標籤
+ 不同檔案 : 外部 .js 文件

<br>

**Sample Code**
```
1. WebSE/jQuerySE/javascript01.html
2. WebSE/jQuerySE/javascript02.html
  + WebSE/jQuerySE/script02.js
3. WebSE/jQuerySE/javascript03.html
```

<br>

jQuery
------
`Javascript 與 jQuery 關係 : 簡潔的 Javascript 庫`

+ 官網 : https://jquery.com/
+ 簡化 : 文檔遍歷、事件處理、動畫和 Ajax 
+ 功能
  + DOM 操作
  + 事件處理
  + AJAX 支持 
  + 動畫
  + 輕量級 `19 KB`
  + 跨瀏覽器支持
+ 下載 : https://jquery.com/download/
  + 【 Download the compressed, production jQuery 3.6.0 】
  + 右鍵 -> 另存連結

<br>

### 比較 : jQuery vs Javascript
```html
<button id="btn-01">Modify Text (Javascript)</button>
<button id="btn-02">Modify Text (jQuery)</button>
<p id="console">Console Text.</p>

<script type="text/JavaScript">
    var btn01 = document.getElementById("btn-01");
    btn01.addEventListener('click',function(){
       var console=document.getElementById("console");
       console.innerHTML = "OwO / Modify Text (Javascript)";
},false)
    
    var btn02 = $("#btn-02");
    btn02.on("click" , function(){
        $("#console").html("OwO / Modify Text (jQuery)")
    })
</script>
```

<br>

**Sample Code :**
```
+ WebSE/jQeerySE/jQuery-Diff.html
```

<br>

### jQuery 使用方法
`官方範例`

```html
<head>
    <script src="assets/jquery-3.6.0.min.js"></script>
    <!--    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>-->
</head>
<body>
    <button class="continue">Button</button>
    <hr>
    <h3>button-container button</h3>
    <div id="button-container">
        <button>Button</button>
        <br>
        <button id="banner-message" style="display:none">banner-message</button>
    </div>
    <hr>
    <button id="ajax-btn">Ajax</button>
    <p id="weather-temp">Console Text</p>

    <script>
        $(function() {
            $("button.continue").html("Next Step...")

            var hiddenBox = $("#banner-message");
            $("#button-container button").on("click", function(event) {
                hiddenBox.show();
            });

            $("#ajax-btn").off("click").on("click" , function(){
                $.ajax({
                    url: "api/getWeather.txt",
                    data: {
                        zipcode: 97201
                    },
                    success: function(result) {
                        $("#weather-temp").html("<strong>" + result + "</strong> degrees");
                    }
                });
            })
        });
    </script>
</body>
```

+ 本地端 : `<script src="assets/jquery-3.6.0.min.js"></script>`
+ 網路端 : `<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>`

<br>

**Sample Code**
```
+ WebSE/jQuerySE/jQuery-Sample.html
```

<br>

jQuery 操作元素
------
**html**
```html
<p id="console">Console Text</p>
<button id="btn01">Button</button>
<script src="jquery-script.js" type="text/javascript"></script>
```

**script**
```javascript
$(function () {
    var $console = $("#console");
    var $btn01 = $("#btn01");
    var count = 0;
    
    $btn01.off('click').on('click' , function(){
        count++;
        $console.text("Count : " + count);
        console.log("Count : " + count);
    });
});
```

+ F12 : 控制台
+ var : $view 
+ click event : `.off('click').on('click', function(){})`
+ `$(function(){});` vs `$(document).ready(function () {});`
    + jQuery 3.0 後推薦第一種，後者則棄用。
    + ref : <https://stackoverflow.com/questions/3528509/document-readyfunction-vs-function>

<br>

**Sample Code**
```
1. WebSE/jQeerySE/jQuery-Script.html
2. WebSE/jQeerySE/jquery-script.js
```

<br>

jQuery - Sample
------
+ Parameter `變數`
+ Function `函數`
+ Selectors `選擇器`
+ Attributes `屬性`
+ CSS Style`樣式`

### Parameter `變數`
```js
// String
var s1 = "This is JavaScript String"
var s2 = 'This is JavaScript String'
var s3 = 'This is "really" a JavaScript String'
var s4 = "This is 'really' a JavaScript String"

// Number : Integer / Float / Double
var n1 = 5350
var n2 = 120.27
var n3 = 0.26

// Boolean
var b1 = true
var b2 = false
var b3 = 1 // true
var b4 = 0 // false
var b5 = "hello" // true
var b6 = "" // false

// Array
var arrNum = [1,2,3,4,5];
var arrStr = ["A" , "B" , "C"];

// Object
var appInfo = {
    id : 1 , 
    name : "jQuerySE" ,
    author : "Enoxs" ,
    version : "1.0.1" ,
    remark : "jQuery - Sample Example"
}

appInfo.version = "1.0.2"
```

<br>

**Sample Code**
```
+ WebSE/jQeerySE/jQeruy-Parameter.html
```

<br>

### Function `函數`
```js
function named() {
    // do some stuff here
    console.log("named()");
}
var handler = function() {
    // do some stuff here
    console.log("handle()");
}

function func01(x) {
    console.log(typeof x, arguments.length);
}

named();
handler();
func01(); //==> "undefined", 0
func01(1); //==> "number", 1
func01("1", "2", "3"); //==> "string", 3
```

**Sample Code**
```
+ WebSE/jQeerySE/jQueryFunction.html
```

<br>

### Selectors `選擇器`
+ Tag `$('button')`
+ ID `$('#mId')`
+ Name `$([name="mName"])`
+ Class `$(.mClass)`
+ Traversing `遍例`

**Tag**

html
```html
<button>Text</button>
```

script
```js
$('button').text("Button");
```

<br>

**ID**

html
```html
<p id="mId">Text</p>
```

script
```js
$('#mId').text("ID");
```

<br>

**Name**
html
```html
<p name="mName">Text</p>
```

script
```js
$('[name="mName"]').text("NAME");
```

<br>

**Class**
html
```html
<p class="mClass">Text</p>
<p class="mGroup">Text</p>
<p class="mGroup">Text</p>
<p class="mGroup">Text</p>
```

script
```js
$('.mClass').text("CLASS");
$(".mGroup*").text("Group");
```

<br>

**Traversing(遍歷)**
+ 多個標籤元素 : 深入階層定位
+ find : 下一階層尋找 `建議寫法`
  1. 區塊分組，較容易看懂 `$div01.find('text01')`
  2. 定位精準，執行動作快 `id > tag > class > attr`
+ eq : 同一階層，計數定位
+ fliter : 同一階層，過濾特定標籤元素
+ next : 通一階層，下一個相對位置
+ prev : 推一階層，上一個相對位置


html
```html

<div id="div01">
    <p id="p01"><span id="span01">Text</span></p>
</div>

<div id="div02">
    <p id="p02"><span id="span02">Text</span></p>
</div>

<div id="div03">
    <p class="p03"><span class="span03">Text</span></p>
</div>
<div id="div04">
    <p class="p04">
        <span class="span041">Text</span>
        <span class="span042">Text</span>
        <span class="span043">Text</span>
    </p>
</div>

<div id="div05">
    <span>Text</span>
    <span id="span05">Text</span>
    <span>Text</span>
</div>
```

script
```js
$("#div01 #p01 #span01").text("DIV>P>SPAN");
$("#div02").find("#p02").find("#span02").text("DIV>P>SPAN"); // find()
$("div").eq(2).find(".p03 .span03").text("DIV>P>SPAN"); // eq()
$("#div04 p span").filter(".span042").text("Test");
$("#div05 #span05").next("span").text("Next");
$("#div05 #span05").prev("span").text("Prev");
```

<br>

**Sample Code**
```
+ WebSE/jQeerySE/jQuerySelector.html
```

<br>

### Attributes `屬性`
+ 一般屬性
  + attr : 取得屬性內容
  + removeAttr : 刪除屬性
+ 類別操作
  + addClass `新增類別`
  + hasClass `判斷類別`
  + removeClass `刪除類別`
  + toggleClass `切換類別`
+ DOM Manipulation `操作`
  + text / html / val
    + text : 純文字
    + html : 標籤元素
    + val : 輸入匡數值
  + append / replaceWith
    + append : 追加內容
    + replaceWith : 取代內容
  + remove / empty
    + remove : 刪除元素
    + empty : 刪除內容
  + before / after
    + before : 向前新增
    + after : 向後新增
  + clone : 克隆元素

<br>

**一般屬性**
+ attr : 取得屬性內容
+ removeAttr : 刪除屬性

html
```html
<h3>Attr / removeAttr </h3>
<p id="title01" name="txt01" class="text">1. Text</p>
<p id="title02">2. Text</p>
```

script
```js
var $title01 = $("#title01");
var titleName01 = $title01.attr("name");
console.log(titleName01)

var $title02 = $("#title02");
$title02.attr("name", "txt02")
$title02.attr("class", "text")
```

<br>

**類別操作**
+ addClass `新增類別`
+ hasClass `判斷類別`
+ removeClass `刪除類別`
+ toggleClass `切換類別`

html
```html
<h3>addClass / hasClass / removeClass / toggleClass</h3>
<p id="title03">3. Text</p>
<p id="title04" name="txt04" class="text">4. Text</p>
<p id="title05" class="text">5. Text</p>
<p id="title06">6. Text</p>
```

script
```js
var $title03 = $("#title03");
$title03.addClass("text");

var $title04 = $("#title04");
$title04.removeAttr("name")
if ($title04.hasClass("text")) {
    $title04.removeClass("text")
}

// Status : Select
var $title05 = $("#title05");
$title05.toggleClass("text");

var $title06 = $("#title06");
$title06.toggleClass("text");
```


<br>

---

    DOM Manipulation `操作`

---

<br>


**text / html / val**
+ text : 純文字
+ html : 標籤元素
+ val : 輸入匡數值

html
```html
<h3>Text / Html / Val</h3>
<div id="group01">Text</div>
<div id="group02">
    <h3>Text</h3>
</div>
<div id="group03">Text</div>

<hr>

<input id="input" type="text" value="Text">
<br>
<input id="output" type="text" value="Text">
```

script
```js
var $group01 = $("#group01");
var text01 = $group01.text();
$group01.text("Group : " + text01);

var $group02 = $("#group02");
var $group03 = $("#group03");
var text02 = $group02.html();
$group03.html(text02);

var $input = $("#input");
var $output = $("#output");

$input.off("change").on("change", function() {
    var text03 = $input.val();
    $output.val(text03);
})
```

<br>

**append / replaceWith**
+ append : 追加內容
+ replaceWith : 取代內容

html
```html
<h3>Append / Replace</h3>
<button id="btnAppend">Append</button>
<p id="append">Text</p>
<p id="replace">Text</p>
```

script
```js
var $btnAppend = $("#btnAppend");
var $append = $("#append");
$btnAppend.off("click").on("click", function() {
    $append.append(" Text");
    $append.append("<h3>HtmlTag<h3>")
})
            
var $replace = $("#replace");
$replace.replaceWith("<h3>Text(Replace)</h3>");
```

<br>

**remove / empty**
+ remove : 刪除元素
+ empty : 刪除內容

html
```html
<h3>Remove / Empty</h3>
<p id="del01">Text</p>
<p id="del02">Text</p>
```

script
```js
var $del01 = $("#del01");
var $del02 = $("#del02");
$del01.remove(); // 刪除元素
$del02.empty(); // 刪除內容
```

<br>

**before / after**
+ before : 向前新增
+ after : 向後新增

html
```html
<h3>Before / After</h3>
<button id="btnAdd">Add</button>
```

script
```js
var $btnAdd = $("#btnAdd");
$btnAdd.off('click').on('click', function() {
    $btnAdd.before("<p>Begin</p>");
    $btnAdd.after("<p>End</p>");
})
```

<br>

**clone : 克隆元素**

html
```html
<h3 id="clone">Clone</h3>
```

script
```js
var $titleClone = $("#title01").clone();
$titleClone.attr("id", "titleClone");
$titleClone.append("(clone)");
$("#clone").after($titleClone);
```

<br>

**Sample Code**
```
+ WebSE/jQeerySE/jQueryAttr.html
```

<br>

### CSS Style`樣式`
**html**
```html
<p id="title01">Text</p>
<p id="title02">Text</p>
<div id="title03">Text</div>
```

**script**
```js
$("#title01").css("background", "yellow");
$("#title02").css({"background": "yellow", "color": "red"});
$("#title03").css("background", "yellow").height("250").width("500");
```

<br>

**Sample Code**
```
+ WebSE/jQeerySE/jQueryCSS.html
```

<br>

jQuery Ajax
------
### WebAPI 
`Spring Boot - Java`

**AppInfoController.java**
```java
@RestController
@RequestMapping("/AppInfo")
public class AppInfoController {
    private final static Logger logger = LoggerFactory.getLogger(AppInfoController.class);

    @Autowired
    private AppInfoApplication appInfoApplication;

    @Value("${app.db.account}")
    private String ac;

    @Value("${app.db.password}")
    private String pw;
    /**
     * Db Setting
     */
    @GetMapping("/DbConfig")
    public String getDbConfig(){
        StringBuffer sb = new StringBuffer(32);
        sb.append("Account = " + ac + "\n");
        sb.append("Password = " + pw + "\n");
        return sb.toString();
    }

    /**
     * This App Info
     */

    @GetMapping("/this")
    public AppInfo getAppInfo(){
        logger.info("getAppInfo()");
        return appInfoApplication.getAppInfo();
    }

    /**
     * 列表
     */
    @GetMapping
    public List<AppInfo> findAll() {
        logger.info("findAll()");
        return appInfoApplication.findAll();
    }

    /**
     * 單筆訊息
     */
    @GetMapping("/{id}")
    public AppInfo findById(@PathVariable("id") Long id) {
        logger.info("findById()");
        return appInfoApplication.findById(id);
    }

    /**
     * 保存數據
     */
    @PostMapping
    public AppInfo create(@RequestBody AppInfo appInfo) {
        logger.info("create()");
        return appInfoApplication.create(appInfo);
    }

    /**
     * 修改數據
     */
    @PutMapping("/{id}")
    public void update(@PathVariable("id") Long id, @RequestBody AppInfo appInfo) {
        logger.info("update()");
        appInfo.setId(id);
        appInfoApplication.update(appInfo);
    }

    /**
     * 刪除數據
     */
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        logger.info("delelet()");
        appInfoApplication.delete(id);
    }
}
```

<br>

**Sample Code**
+ <https://gitlab.com/GammaRayStudio/Tutor/WebAPI.git>

**document**
```
+ SQL-Doc/docker-mysql.md
```

**IntelliJ , Plugin**
+ Restful Tool

<br>

### Ajax : RESTful
**html**
```html
<h3>GET : AppInfo/this</h3>
<button id="btn01">Ajax</button>
<hr>

<h3>GET : AppInfo/1 (Select)</h3>
<button id="btn02">Ajax</button>
<hr>

<h3>POST : AppInfo (Create)</h3>
<button id="btn03">Ajax</button>
<hr>

<h3>PUT : AppInfo (Update)</h3>
<button id="btn04">Ajax</button>
<hr>

<h3>DELETE : AppInfo (Delete)</h3>
<button id="btn05">Ajax</button>
<hr>

<p id=result></p>
```

**script**
```js
var $result = $("#result");

$("#btn01").off("click").on("click", function() {
    $.ajax({
        url: "AppInfo/this",
        type: "GET",
        data: {
            // zipcode: 97201
        },
        success: function(result) {
            $("#result").html("【result】<br>");
            for (var key in result) {
                $result.append("key : " + key + " , value : " + result[key] + "<br>")
            }
        }
    });
})
$("#btn02").off("click").on("click", function() {
    $.ajax({
        url: "AppInfo/1",
        type: "GET",
        data: {
            // id : 1
        },
        success: function(result) {
            $("#result").html("【result】<br>");
            for (var key in result) {
                $result.append("key : " + key + " , value : " + result[key] + "<br>")
            }
        }
    });
})

$("#btn03").off("click").on("click", function() {
    var para = {
        name: "jQuerySE",
        version: "1.0.1",
        author: "Enoxs",
        date: "2021-07-24",
        remark: "jQuery - Sample Example"
    }
    $.ajax({
        url: "AppInfo",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(para),
        success: function(result) {
            $("#result").html("【result】<br>");
            for (var key in result) {
                $result.append("key : " + key + " , value : " + result[key] + "<br>")
            }
        }
    });
})

$("#btn04").off("click").on("click", function() {
    var id = 10
    var para = {
        name: "jQuerySE",
        version: "1.0.5",
        author: "Enoxs",
        date: "2021-07-24",
        remark: "jQuery - Sample Code"
    }
    $.ajax({
        url: "AppInfo/" + id,
        type: "PUT",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(para),
        success: function(result) {
            $("#result").html("【result】<br>");
            for (var key in result) {
                $result.append("key : " + key + " , value : " + result[key] + "<br>")
            }
        }
    });
})

$("#btn05").off("click").on("click", function() {
    var id = 10;
    $.ajax({
        url: "AppInfo/" + id,
        type: "DELETE",
        success: function(result) {
            $("#result").html("【result】<br>");
            for (var key in result) {
                $result.append("key : " + key + " , value : " + result[key] + "<br>")
            }
        }
    });
})

$("#btn06").off("click").on("click", function() {
    var postUrl = 'AppInfo'
    var para = {
        name: "jQuerySE",
        version: "1.0.1",
        author: "Enoxs",
        date: "2021-07-24",
        remark: "jQuery - Sample Example"
    }
    $.post(postUrl, $.param({
        jsonString: JSON.stringify(para)
    }), function(res) {
        var jsonObj = JSON.parse(resp.data);
        console.log('res: ', res);
        console.log('jsonObj: ', jsonObj);
    });
})

```

<br>

jQuery UI
------
+ 官網 : https://jqueryui.com/
+ 風格 : https://jqueryui.com/themeroller/
+ 下載 : https://jqueryui.com/download/

**html**
```html
<h2 id="hello">Hello</h2>

<button id="show">SHOW</button>
<button id="hide">HIDE</button>
<button id="toggle">TOGGLE</button>

<p id="console">Text</p>
```

**script**
```js
var $hello = $("#hello");
var $console = $("#console");

$("#show").off('click').on('click',function(){
    $hello.show();
    $hello.show(1000);
    $hello.show(1000 , function(){
        $console.text("Show");
    });
})

$("#hide").off('click').on('click',function(){
    $hello.hide();
    $hello.hide(1000);
    $hello.hide(1000, function(){
        $console.text("Hide");
    });
})

$("#toggle").off('click').on('click',function(){
    $hello.toggle();
    $hello.toggle(1000);
    $hello.toggle(1000, function(){
        $console.text("Toggle");
    });
})
```

<br>

### Sample 01 : Template

**Sample Code**
```
+ WebSE/jQeerySE/jQueryUI-Sample01.html
```

<br>

### Sample 02 : Handle
+ selectmenu
+ accordion

**html**
```html
<select id="selectmenu">
    <option selected="selected">001</option>
    <option>002</option>
    <option>003</option>
    <option>004</option>
    <option>005</option>
</select>
<button id="btnAjax01" class="ui-button ui-widget ui-corner-all">Ajax</button>
```

**script**
```js
var $selectmenu = $("#selectmenu");
$selectmenu.selectmenu();
$("#btnAjax01").off("click").on("click", function() {
    var lstData = ["A", "B", "C", "D", "E"];
    $selectmenu.empty();
    $selectmenu.append('<option>請選擇</option>');
    $.each(lstData, function(index, value) {
        $selectmenu.append('<option value="' + value + '">' + value + '</option>');
    });
    $selectmenu[0].selectedIndex = 0; //不知為何要加[0]
    $selectmenu.selectmenu("refresh", true);
})
```
+ empty() : 清空
+ append() : 重新新增元素
+ $selectmenu[0].selectedIndex : 指定元件選擇位置
  + `$("")加[0]的意思是把jQuery物件轉為DOM物件。這樣子jQuery物件才能使用DOM底下的selectedIndex方法`
  + ref : <https://blog.xuite.net/dizzy03/murmur/48212886>
+ $selectmenu.selectmenu("refresh", true) : 標籤元素重建完成後，要再重新刷新畫面 

<br>

**Sample Code**
```
+ WebSE/jQeerySE/jQueryUI-Sample02.html
```

<br>

jQuery Plugin
------
### Moment
+ 官網 : https://momentjs.com/

**Sample Code**
```
+ WebSE/jQeerySE/Moment.html
```

<br>

### DataTable
+ 官網 : https://datatables.net/

**Sample Code**
```
1. WebSE/jQeerySE/DataTable.html
2. WebSE/jQeerySE/DataTable.js
3. WebSE/jQeerySE/DataTableUtil.js
```

### 主軸
```
jQuery 有很多插件可以使用，有想要呈現什麼特殊效果可以先上網搜尋看看。
```

<br>

Architecture
------
+ initPageView() : 初始化整個頁面，會放到最後在呼叫。
+ ApplyModule `ApplyDiv`
  + initApplyView() : 初始化功能區塊，畫面事件與資料傳輸物件統一在這裡控制
  + initApplyViewEvent() : 初始化功能畫面事件
  + initApplyDataAjax() : 初始化功能資料傳輸
+ QueryModule `QueryDiv`
  + initQueryView() : `var view ; var ajxa;`
  + initQueryViewEvent() : `var $btn ; $btn.off('click').on('click',function(){})`
  + initQueryDataAjax() : `$ajax({url : "" , type : "" , data : {} , success: function(){} });`

**html**
```html
<h1 id="headline">Architecture</h1>

<div id="applyDiv">
    <h2 id="titleApply">Title : Apply</h2>
    <p id="textApply">Content : Text</p>
    <button id="btnApply">Action</button>
    <button id="btnNext">Next Page</button>
</div>

<div id="queryDiv" style="display: none;">
    <h2 id="titleQuery">Title : Query</h2>
    <p id="textQuery">Content : Text</p>
    <button id="btnQuery">Action</button>
    <button id="btnPrev">Prev Page</button>
</div>
```

**script**
```js
// Apply
var applyCount = 0;

// Query
var queryCount = 0;

var initPageView = function () {
    initApplyView();
}

// ApplyDiv
var initApplyView = function () {
    var view = initApplyViewEvent();
    var ajax = initApplyDataAjax();
    view.setBtnApplyEvent(ajax);
    view.show();
}

var initApplyViewEvent = function () {
    var $divApply = $("#applyDiv");
    var $titleApply = $divApply.find("#titleApply");
    var $textApply = $divApply.find("#textApply");
    var $btnApply = $divApply.find("#btnApply");
    var $btnNext = $divApply.find("#btnNext");

    var view = {
        show : function(){
            $divApply.show();
        },
        hide : function(){
            $divApply.hide();
        },
        setTitleApply : function(msg){
            $titleApply.text(msg);
        },
        setTextApply : function(msg){
            $textApply.text(msg)
        },
        setBtnApplyEvent : function(ajax){
            $btnApply.off("click").on("click" , function(){
                ajax.get(view.setTextApply);
            })
        },
        setBtnNextEvent : function(){
            $btnNext.off("click").on("click" , function(){
                $divApply.hide();
                initQueryView();
            })
        }
    }
    view.setBtnNextEvent();
    return view;
}

var initApplyDataAjax = function () {
    var ajax = {
        get: function (setTextApply) {
            // do something about ajax
            applyCount ++;
            var text = "OwO / Ajax Data (" + applyCount + ")";
            setTextApply(text);
        }
    }
    return ajax;
}

// QueryDiv
var initQueryView = function () {
    var view = initQueryViewEvent();
    var ajax = initQueryDataAjax();
    view.setBtnQueryEvent(ajax);
    view.show();
}

var initQueryViewEvent = function () {
    var $queryDiv = $("#queryDiv");
    var $titleQuery = $queryDiv.find("#titleQuery");
    var $textQuery = $queryDiv.find("#textQuery");
    var $btnQuery = $queryDiv.find("#btnQuery");
    var $btnPrev = $queryDiv.find("#btnPrev");

    var view = {
        show : function(){
            $queryDiv.show();
        },
        hide : function(){
            $queryDiv.hide();
        },
        setTitleQuery : function(msg){
            $titleQuery.text(msg);
        },
        setTextQuery : function(msg){
            $textQuery.text(msg)
        },
        setBtnQueryEvent : function(ajax){
            $btnQuery.off("click").on("click" , function(){
                ajax.get(view.setTextQuery);
            })
        },
        setBtnPrevEvent : function(){
            $btnPrev.off("click").on("click" , function(){
                $queryDiv.hide();
                initApplyView();
            })
        }
    }
    view.setBtnPrevEvent();

    $queryDiv.css("background" , "yellow")

    return view;
}

var initQueryDataAjax = function () {
    var ajax = {
        get: function (setTextQuery) {
            // do something about ajax
            $.ajax({
                url: "api/AjaxData.json",
                type: "GET",
                data: {
                    id : 1
                },
                success: function(result) {
                    for (var key in result) {
                        console.log("key : " + key + " , value : " + result[key] + "<br>")
                    }
                    setTextQuery("id : " + result.id + " , name : " + result.name)
                }
            });
        }
    }
    return ajax;
}
```

<br>


**Sample Code**
```
+ WebSE/jQuerySE/Architecture.html
+ WebSE/jQuerySE/architecture.js
+ WebSE/jQuerySE/api/AjaxData.json
```

<br>

Reference
------
### jQuery
<https://jquery.com/>

### TutorialsPoint - Html , Javascript
<https://www.tutorialspoint.com/html/html_javascript.htm>

### TutorialsPoint - Javascript
<https://www.tutorialspoint.com/javascript/index.htm>

### TutorialsPoint - jQuery
<https://www.tutorialspoint.com/jquery/index.htm>




