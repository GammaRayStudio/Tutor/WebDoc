Apache Tomcat
======


Wiki : Tomcat
------
<https://zh.wikipedia.org/wiki/Apache_Tomcat>

<br>

Downloads
------
<https://tomcat.apache.org/download-80.cgi>

<br>

Java 環境
------
`未設置 java 環境變數，啟動時會發生閃退`

+ JAVA_HOME : `C:\..\Java\jdk\`
+ CATALINA_HOME : `C:\..\Java\tomcat\`

<br>

### OpenJDK
+ 32 bit : https://jdk.java.net/java-se-ri/8-MR3
+ 64 bit : https://jdk.java.net/archive/

<br>

啟動與停止
------
### Win
+ 啟動 : tomcat/bin/startup.bat
+ 停止 : tomcat/bin/shutdown.bat

<br>


### Mac
+ 啟動 : tomcat/bin/startup.sh
+ 停止 : tomcat/bin/shutdown.sh

<br>


測試
------
### 瀏覽器訪問
+ <http://localhost:8080>
+ <http://127.0.0.1:8080>

<br>

### 區網地址
+ 192.168.X.X:8080

**指令**

    ipconfig

<br>

### 網頁測試
`tomcat/webapps/ROOT`

**Sample.html**
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    <h1>WebPage : Sample</h1>
</body>
</html>
```

**訪問**
+ <http://localhost:8080/Sample.html>
+ <http://127.0.0.1:8080/Sample.html>
+ 192.168.X.X:8080/Sample.html

<br>

### 其他網頁伺服器
+ Wiki : https://zh.wikipedia.org/wiki/%E7%B6%B2%E9%A0%81%E4%BC%BA%E6%9C%8D%E5%99%A8

**XAMPP**

`集合安裝包 : Apache + MariaDB + PHP + Perl`

+ 網址 : <https://www.apachefriends.org/zh_tw/index.html>
+ 網頁 : `xampp/htdocs/`
+ 訪問 : <http://localhost> `port:80`







