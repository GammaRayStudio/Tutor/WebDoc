XAMPP
======
`集合安裝包 : Apache + MariaDB + PHP + Perl`

### 官網下載
+ <https://www.apachefriends.org/zh_tw/index.html>

### htdocs 資料夾
`xampp/htdocs/`

**home.html**
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Home</title>
</head>
<body>
    <h1>My website</h1>
</body>
</html>
```

### 網頁測試 `Port:80`
+ <http://localhost/home.html>
+ <http://127.0.0.1/home.html>


### 其他網頁伺服器
+ AppServ : https://www.appserv.org/en/
+ Tomcat : <https://tomcat.apache.org/download-80.cgi>
+ Wiki : https://zh.wikipedia.org/wiki/%E7%B6%B2%E9%A0%81%E4%BC%BA%E6%9C%8D%E5%99%A8



