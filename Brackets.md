Web 開發工具 : Adobe Brackets
------

![001](assets/brackets/002.adobe-bracket.jpeg)

### 最新消息
`2021 的 9 月 1 日，Adobe 停止支援。(官方推薦 Visual Studio Code)`

![003](assets/brackets/003.web-official-info.jpeg)
(此工具是開源專案，有開源社群的維護與更新，應該還可以再使用一段時間。)

### 下載
`官網上已經找不到下載點，必須到 GitHub 頁面的 Releases 下載`

+ 官方網站 : http://brackets.io/
+ GitHub : https://github.com/adobe/brackets
+ Release : https://github.com/adobe/brackets/releases

### 擴充
`其中一個選擇 Brackets 因素，是因為很多的插件可以使用。`

通常第一次啟動會點擊右側下方第二個圖示，開啟「擴充功能管理員」:

![004](assets/brackets/004.extension-manager.jpeg)

**已安裝插件(參考):**

+ Custom Work
+ Emmet
+ Beautify
+ Brackets Markdown Preview
+ Brackets Icons

<br>

其中兩個最重要的插件 : **Custom Work** 與 **Emmet**

<br>

**Extension : Custom Work**

改變工作區的佈局，可以增強分頁的功能與使用。

<br>

**Extension : Emmet**

前端開發的好用工具，在其他的網頁開發工具中，應該都可以找的到這個擴充。`(VSCode、Sublime)`

工具的使用方法，就是透過語法的縮寫 加上 Tab 按鍵，自動完成 html 的標籤格式 :

![005](assets/brackets/005.emmet.jpeg)

<br>

Emmet 官方速查表 : 

![006](assets/brackets/006.emmet-cheat-sheet.jpeg)
+ url : <https://docs.emmet.io/cheat-sheet/>

<br>

特色
------
+ 開源專案工具
+ 很多擴充插件
+ 即時預覽功能

<br>

官方推薦的 VSCode
------
+ VSCode : <https://code.visualstudio.com/>
+ Wiki : <https://zh.wikipedia.org/zh-tw/Visual_Studio_Code>






