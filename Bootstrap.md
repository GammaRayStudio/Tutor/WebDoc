Bootstrap 入門指南
======
[`Youtube: 網頁設計 入門 : 如何使用 Bootstrap 與 Github Pages 製作 個人網站 ? `](https://youtu.be/ctr6eo9dL-A)

<br>

Bootstrap 範例
------
### Gamma Ray 軟體工作室
+ <https://gamma-ray-studio.github.io/zh-cht/index.html>

### 物聯網 Side Proejct - Tunffli
+ https://rhzenoxs.github.io/Tunffli-View/

<br>

為什麼要選擇 Bootstrap ?
------
`簡單、好看、支援 RWD`

<br>

**簡單與好看的做法**

最原始、簡單的做法，撰寫好一個 html 包含 css 的網頁後，上傳伺服器。

簡單是簡單，只不過如果沒有 UI 設計師的支援光靠工程師自己的美感，就只會跟大學時期繳交的作業差不多。

![013](assets/bootstrap/013.homework.jpeg)

```
如此情況，要獨立的完成這項工作，只能選擇現成的模板或者框架。
```

<br>

**gitlab pages example**

經過一番查找，發現 gitlab pages example 收錄了許多現成、可以快速建置的網頁模板。

![015](assets/bootstrap/015.gitlab-example.jpeg)

+ url : <https://gitlab.com/pages>



<br>

搜尋時常看到的關鍵字: hexo、hugo、Jekyll，成品看起來是都不錯

![016](assets/bootstrap/016.hugo-example.jpeg)

<br>

只不過我發現這些模板各自使用 NodeJS、Go、Ruby 程式語言開發，安裝時需要輸入各種指令或者配置各種參數

![017](assets/bootstrap/017.hexo-install.jpeg)

**對我而言，美感是有了但不夠簡單。**

<br>

**Wix 或 WordPress**

![018](assets/bootstrap/018.wix-wordpress.jpeg)

這種用拖拉方式建立網站的方法，對於已經熟悉程式語言的工程師來說，反而會失去原本對網頁細節調整的優勢。

而且時間上整體的流程，從註冊後各種參數的設置主題、外掛的選擇以及最終外觀細節的調整，我認為並不會比較短。

更何況熟悉網站的操作，本身也是需要一定的學習成本。

![018](assets/bootstrap/019.wix-flow.jpeg)

<br>

**因此，純粹的前端框架 Bootstrap 就是一個合乎需求的方案**

這個方案的使用方式，就跟原始的做法一樣，撰寫完 html 的網頁後直接丟上伺服器。

而且Bootstrap 還多了 RWD 響應式網頁的優勢，完成主體的頁面建置後順便就支援手機畫面的瀏覽。

![020](assets/bootstrap/020.rwd-web-page.jpeg)

<br>

### 下載

**Bootstrap 官網 :** <https://getbootstrap.com/>


![021](assets/bootstrap/021.bootstrap-download.jpeg)

+ Examples > Downloads Example 

<br>

下載解壓縮後，其中最重要的資料夾是 **assets/dist/** 資料夾，這裡面是包含 bootstrap 框架所有的 **css 與 javascript 程式碼**。

複製整個 assets 資料夾到網站的專案路徑，然後刪除用不到的。只留下 **bootstrap.bundle.min.js 與 bootstrap.min.css** 這兩個檔案。

![022](assets/bootstrap/022.assets-dist.jpeg)

<br>

### 範例

比對 bootstrap-example 的資料夾以及 Bootstrap 網頁的下載頁面，兩者名稱互相對應

![023-1](assets/bootstrap/023-1.example.jpeg)

<br>

**Album 專輯範例**

網頁呈現的畫面 與 本地端 Album 資料夾 index 檔案，開啟的畫面一樣。

![023-2](assets/bootstrap/023-2.example.jpeg)

換句話說，bootstrap-example 資料夾的檔案也是**官方網站範例網頁的原始檔**。


```
知道兩者關係後，接下來就是要在範例中找到喜歡的佈局，然後調整成想要的樣子。
```

<br>

**例如 Gamma Ray 軟體工作室官方網站的三個頁面**

首頁 & Jumbotron

![024-1](assets/bootstrap/024-1.index-jumbotron.jpeg)

100App+ & Album

![024-2](assets/bootstrap/024-2.100App+album.jpeg)

Mark Account & Carousel

![024-3](assets/bootstrap/024-3.mark-account-carousel.jpeg)

<br>

### 修改的細節
`以 Jumbotron 範例的調整進行說明`

比較畫面:

![025](assets/bootstrap/025.diff-view.jpeg)

+ 頂部區塊 : 導航欄切換不同的語系
+ 中間區塊 : 背景顏色為黑色，按鈕的部份較大，還有連結的網站圖示
+ 底部區塊 : 撰寫了許多文字，呈現工作室的資訊

<br>

**修改的第一步**

將 Jumbotron 範例的 index 檔案，複製到專案。

使用瀏覽器開啟，渲染的結果可以發現雖然文字部分還是一樣，但整體的佈局樣式已經跑板。

![026](assets/bootstrap/026.clone-preview.jpeg)

這是因為導入 css 樣式的 link 標籤，使用的是相對路徑，將路徑中上一層的位置刪除，重新整理就會顯示正常。

```diff
- <link href="../assets/dist/css/bootstrap.min.css" rel="stylesheet">
+ <link href="assets/dist/css/bootstrap.min.css" rel="stylesheet">
```

<br>

**Jumbotron 範例的 html 文字結構 :**
```html
<!doctype html>
<html lang="en">
  <head>
    <!-- something ... -->
    <title>Jumbotron example · Bootstrap v5.0</title>
    <!-- Bootstrap core CSS -->
	<link href="assets/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
      <!-- something ... -->
    </style>
  </head>
  <body>
<main>
  <div class="container py-4">
    <header class="pb-3 mb-4 border-bottom">
      <!-- something ... -->
    </header>

    <!-- something ... -->

    <footer class="pt-3 mt-4 text-muted border-top">
      &copy; 2021
    </footer>
  </div>
</main>    
  </body>
</html>
```

有了之前的 html 與 css 語法說明，應該能夠理解 head 標籤 style 標籤 與 body 標籤，各自具備什麼功能。


不過 在 body 的內容區塊，之前尚未說明的，還有 main、header、footer 等標籤區塊。

這些並不是 bootstrap 自定義的格式，而是 html5 最新規格的語意標籤

```
HTML 5 語意標籤 :

<header> : 網頁的標頭，放置網站標題
<nav> : 網頁的選單、導覽。
<main> : 網頁的主要內容。
<aside> : 網頁的側欄、附加內容。
<article> : 一篇文章內容。
<section> : 自訂區塊。
<footer> : 網頁的頁尾，放置聯絡方式、著作權宣告等等。
<mark> : 強調一小塊內容。
<time> : 顯示日期時間。

(參考來源 : https://training.pada-x.com/docs/article.jsp?key=html5-semantic-elements)
```

<br>

對應畫面與程式碼 :

![027](assets/bootstrap/027.example-view.jpeg)

```html
<head>
	<!-- Bootstrap core CSS -->
	<link href="assets/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
	<main>
	  <div class="container py-4">
	    <header class="pb-3 mb-4 border-bottom">
	      <a href="/" class="d-flex align-items-center text-dark text-decoration-none">
	        <svg xmlns="http://www.w3.org/2000/svg" width="40" height="32" class="me-2" viewBox="0 0 118 94" role="img"><title>Bootstrap</title><path fill-rule="evenodd" clip-rule="evenodd" d="M24.509 0c-6.733 0-11.715 5.893-11.492 12.284.214 6.14-.064 14.092-2.066 20.577C8.943 39.365 5.547 43.485 0 44.014v5.972c5.547.529 8.943 4.649 10.951 11.153 2.002 6.485 2.28 14.437 2.066 20.577C12.794 88.106 17.776 94 24.51 94H93.5c6.733 0 11.714-5.893 11.491-12.284-.214-6.14.064-14.092 2.066-20.577 2.009-6.504 5.396-10.624 10.943-11.153v-5.972c-5.547-.529-8.934-4.649-10.943-11.153-2.002-6.484-2.28-14.437-2.066-20.577C105.214 5.894 100.233 0 93.5 0H24.508zM80 57.863C80 66.663 73.436 72 62.543 72H44a2 2 0 01-2-2V24a2 2 0 012-2h18.437c9.083 0 15.044 4.92 15.044 12.474 0 5.302-4.01 10.049-9.119 10.88v.277C75.317 46.394 80 51.21 80 57.863zM60.521 28.34H49.948v14.934h8.905c6.884 0 10.68-2.772 10.68-7.727 0-4.643-3.264-7.207-9.012-7.207zM49.948 49.2v16.458H60.91c7.167 0 10.964-2.876 10.964-8.281 0-5.406-3.903-8.178-11.425-8.178H49.948z" fill="currentColor"></path></svg>
	        <span class="fs-4">Jumbotron example</span>
	      </a>
	    </header>

	    <div class="p-5 mb-4 bg-light rounded-3">
	      <div class="container-fluid py-5">
	        <h1 class="display-5 fw-bold">Custom jumbotron</h1>
	        <p class="col-md-8 fs-4">Using a series of utilities, you can create this jumbotron, just like the one in previous versions of Bootstrap. Check out the examples below for how you can remix and restyle it to your liking.</p>
	        <button class="btn btn-primary btn-lg" type="button">Example button</button>
	      </div>
	    </div>

	    <div class="row align-items-md-stretch">
	      <div class="col-md-6">
	        <div class="h-100 p-5 text-white bg-dark rounded-3">
	          <h2>Change the background</h2>
	          <p>Swap the background-color utility and add a `.text-*` color utility to mix up the jumbotron look. Then, mix and match with additional component themes and more.</p>
	          <button class="btn btn-outline-light" type="button">Example button</button>
	        </div>
	      </div>
	      <div class="col-md-6">
	        <div class="h-100 p-5 bg-light border rounded-3">
	          <h2>Add borders</h2>
	          <p>Or, keep it light and add a border for some added definition to the boundaries of your content. Be sure to look under the hood at the source HTML here as we've adjusted the alignment and sizing of both column's content for equal-height.</p>
	          <button class="btn btn-outline-secondary" type="button">Example button</button>
	        </div>
	      </div>
	    </div>

	    <footer class="pt-3 mt-4 text-muted border-top">
	      &copy; 2021
	    </footer>
	  </div>
	</main>
</body>
```
+ header 標頭 : svg 標籤的圖片 與 span 標籤的文字，組成一個 a 標籤的超連結。
+ main 主體 : 上下切分成兩個 div 區塊
	+ 上方 div : h1 標題、p 段落文字、button 按鈕組合
	+ 下方 div : 兩個左與右的區塊，分別對應上與下的 div
		+ 左側 div : h2 標題、p 段落文字、button 按鈕
		+ 右側 div : h2 標題、p 段落文字、button 按鈕
+ footer 頁尾 : 版權符號加上 2021 的文字

```
每一標籤中 class 屬性套用 css 的樣式，就是剛才 link 標籤 bootstrap.min.css 檔案中，已經定義好的樣式內容。
```

<br>

### 上中下三個區塊的調整，範例說明 bootstrap 幾個重要概念
1. 標頭部分
2. 中間主體
3. 底部文字

<br>

**標頭部分**

我需要的並不是一個純粹的網站標題，而是一個可以切換多國語系的導航選單。

在 Examples 頁面瀏覽，發現 Headers 範例部分由上方數下來第五個導航列看起來符合需求。

![028-1](assets/bootstrap/028-1.header-example.jpeg)

<br>

開啟原始檔案，有了剛才畫面與程式碼的對照經驗，我們知道要尋找的內容是 header 標籤。

不過這邊 header 標籤不只一個，所以除了用計數的方式尋找，也可以透過畫面的文字定位。

![028-2](assets/bootstrap/028-2.header-example.jpeg)

```html
<header class="p-3 mb-3 border-bottom">
    <div class="container">
      <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
        <a href="/" class="d-flex align-items-center mb-2 mb-lg-0 text-dark text-decoration-none">
          <svg class="bi me-2" width="40" height="32" role="img" aria-label="Bootstrap"><use xlink:href="#bootstrap"/></svg>
        </a>

        <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
          <li><a href="#" class="nav-link px-2 link-secondary">Overview</a></li>
          <li><a href="#" class="nav-link px-2 link-dark">Inventory</a></li>
          <li><a href="#" class="nav-link px-2 link-dark">Customers</a></li>
          <li><a href="#" class="nav-link px-2 link-dark">Products</a></li>
        </ul>

        <form class="col-12 col-lg-auto mb-3 mb-lg-0 me-lg-3">
          <input type="search" class="form-control" placeholder="Search..." aria-label="Search">
        </form>

        <div class="dropdown text-end">
          <a href="#" class="d-block link-dark text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
            <img src="https://github.com/mdo.png" alt="mdo" width="32" height="32" class="rounded-circle">
          </a>
          <ul class="dropdown-menu text-small" aria-labelledby="dropdownUser1">
            <li><a class="dropdown-item" href="#">New project...</a></li>
            <li><a class="dropdown-item" href="#">Settings</a></li>
            <li><a class="dropdown-item" href="#">Profile</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="#">Sign out</a></li>
          </ul>
        </div>
      </div>
    </div>
  </header>
 ```

找到目標的程式碼後，複製貼到原本的 header 代碼。

<br>

而為了避免導航列的樣式有所遺漏，最好是將 navbar.css 與 link 標籤也一併複製。

![029](assets/bootstrap/029.navbar-css.jpeg)

```html
<head>
	<!-- Custom styles for this template -->
	<link href="headers.css" rel="stylesheet">
</head>
```

<br>

儲存後，網頁標頭的導航列已經出現。不過仔細查看發現三個問題:

![030](assets/bootstrap/030.nav-issue.jpeg)

+ 問題一 : ICON 圖示的部分 沒有出現
+ 問題二 標頭的導航列 沒有貼合在頂部
+ 問題三 點擊最右側的選單 畫面沒有回應

```
這邊這三個問題的修正方法，也剛好分別代表三種除錯思路。
```

<br>

**問題一 : ICON 消失，因為範例中使用的是 svg 向量圖示**

原始的程式碼，定義在 main 標籤上方的 svg 標籤裡面

將 svg 標籤刪除後，改使用 img 標籤，套用我們自己的 logo 圖片，然後再順便使用 span 標籤 與 style 屬性，添加標題名稱

```diff
 <header class="pb-3 mb-4 border-bottom">
	<a href="/" class="d-flex align-items-center text-dark text-decoration-none">
-		<svg xmlns="http://www.w3.org/2000/svg" width="40" height="32" class="me-2" viewBox="0 0 118 94" role="img"><title>Bootstrap</title><path fill-rule="evenodd" clip-rule="evenodd" d="M24.509 0c-6.733 0-11.715 5.893-11.492 12.284.214 6.14-.064 14.092-2.066 20.577C8.943 39.365 5.547 43.485 0 44.014v5.972c5.547.529 8.943 4.649 10.951 11.153 2.002 6.485 2.28 14.437 2.066 20.577C12.794 88.106 17.776 94 24.51 94H93.5c6.733 0 11.714-5.893 11.491-12.284-.214-6.14.064-14.092 2.066-20.577 2.009-6.504 5.396-10.624 10.943-11.153v-5.972c-5.547-.529-8.934-4.649-10.943-11.153-2.002-6.484-2.28-14.437-2.066-20.577C105.214 5.894 100.233 0 93.5 0H24.508zM80 57.863C80 66.663 73.436 72 62.543 72H44a2 2 0 01-2-2V24a2 2 0 012-2h18.437c9.083 0 15.044 4.92 15.044 12.474 0 5.302-4.01 10.049-9.119 10.88v.277C75.317 46.394 80 51.21 80 57.863zM60.521 28.34H49.948v14.934h8.905c6.884 0 10.68-2.772 10.68-7.727 0-4.643-3.264-7.207-9.012-7.207zM49.948 49.2v16.458H60.91c7.167 0 10.964-2.876 10.964-8.281 0-5.406-3.903-8.178-11.425-8.178H49.948z" fill="currentColor"></path></svg>
-		<span class="fs-4">Jumbotron example</span>
+		<img src="assets/img/gamma-ray-logo2.png" alt="" width="36" height="36" class="me-2" viewBox="0 0 118 94">
+		<span class="fs-4" style="color: red;font-weight: bold;">Gamma Ray Studio</span>
	</a>
 </header>
```

重新整理後，圖片與標題 就會出現

![031-1](assets/bootstrap/031-1.fix-issue.jpeg)

除錯思路:
```
檢查原始檔案的頂部，看一下是不是有東西被遺漏，
有缺少的部分找東西替代或者用原始碼補上。
```

<br>

**問題二 : 功能列沒有貼合頂部，因為 main 標籤與 header 標籤的位置順序導致**

此問題如果只在 header 的範例中，查看原始碼是找不到原因的，因為所有的內容都在 main 標籤裡面。

另外一個更好的方案，觀察同樣有狀態列的 Carousel 範例，他的 header 標籤 是在 main 標籤 的上方。

![031-2](assets/bootstrap/031-2.fix-issue.jpeg)

所以同樣在程式碼中 調整位置，重新整理後位置確實已經貼合在頂部。

```diff
 <body>
- <main>
    <header class="p-3 mb-3 border-bottom">
    <div class="container">
      <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
        <a href="/" class="d-flex align-items-center mb-2 mb-lg-0 text-dark text-decoration-none">
          <img src="assets/img/gamma-ray-logo2.png" alt="" width="36" height="36" class="me-2" viewBox="0 0 118 94">
                    <span class="fs-4" style="color: red;font-weight: bold;">Gamma Ray Studio</span>
        </a>

        <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
          <li><a href="#" class="nav-link px-2 link-secondary">Overview</a></li>
          <li><a href="#" class="nav-link px-2 link-dark">Inventory</a></li>
          <li><a href="#" class="nav-link px-2 link-dark">Customers</a></li>
          <li><a href="#" class="nav-link px-2 link-dark">Products</a></li>
        </ul>

        <form class="col-12 col-lg-auto mb-3 mb-lg-0 me-lg-3">
          <input type="search" class="form-control" placeholder="Search..." aria-label="Search">
        </form>

        <div class="dropdown text-end">
          <a href="#" class="d-block link-dark text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
            <img src="https://github.com/mdo.png" alt="mdo" width="32" height="32" class="rounded-circle">
          </a>
          <ul class="dropdown-menu text-small" aria-labelledby="dropdownUser1">
            <li><a class="dropdown-item" href="#">New project...</a></li>
            <li><a class="dropdown-item" href="#">Settings</a></li>
            <li><a class="dropdown-item" href="#">Profile</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="#">Sign out</a></li>
          </ul>
        </div>
      </div>
    </div>
  </header>
+ <main>
 </main>
 </body
```

除錯思路:
```
原始的程式碼，如果看不出問題時，直接去參照已經正確的完整範例，觀察兩者之間 有什麼不一樣
然後調整、測試，你想要的結果，通常都會在這一過程中被嘗試出來。
```

<br>

**問題三 : 點擊選單沒有反應，因為 bootstrap 動態網頁的程式碼沒有導入**

缺少 header 範例底部的 script 標籤

headers/index.html : 

```html
<!-- ... 略 ... -->
	<script src="../assets/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
```

複製到程式碼，相對路徑的部分同樣需要調整。

```diff
 <!-- ... 略 ... -->
+    <script src="assets/dist/js/bootstrap.bundle.min.js"></script>
-    <script src="../assets/dist/js/bootstrap.bundle.min.js"></script>
  </body>
 </html>
```

完成後，重新整理選單展開正常 :

![031-3](assets/bootstrap/031-3.fix-issue.jpeg)

此問題也是之前基本 html 語法 button 標籤提過的動態網頁概念，副檔名為 .js 的檔案就是 bootstrap 的 javascript 腳本。

```
之所以要放在最後面，是因為希望前面的 html 標籤都加載完成後，才執行元件綁定的動作。
這樣可以避免，因為時間差找不到畫面元素，造成動畫可能失效情況發生。
```

除錯思路有二 :
```
1. 除了之前原始檔案頂部的檢查，最好也檢查一下底部有沒有遺漏
2. 如果跟畫面互動有關的問題，大部分都會跟 javascript 的東西有關，可以查看是不是缺少了 script 的標籤或者是 .js 的檔案 
```

<br>

**以上三個問題的解決方法，也是三種最常見的排查方案 :**

```
之後如果自己在調整網頁問題時，先檢查原始檔案的頭尾然後在比對其他範例，通常大部分的問題都會在這一過程中找到解決的方法。
```

<br>

**中間主體**

![032](assets/bootstrap/032.modify-text.jpeg)

最基本的文字修改，有了之前畫面與程式碼的對應，應該可以知道 :
+ 標題的部分 -> 修改 h1 與 h2 的標籤內容
+ 下方的文字 -> 修改 段落文字 p 的標籤內容

<br>

**除了文字的修改，還要再透過三個部分的調整，說明 Bootstrap 的使用方式與基本概念 :**

**調整一 : 將這三個區塊都變成左下角黑底白字的樣式**

![033](assets/bootstrap/033.adjust-div.jpeg)

最快的方法，查看這一個區塊，了解這個 div 的 class 屬性都是使用了哪些樣式

![034](assets/bootstrap/034.class-style.jpeg)

當中 text-white 與 bg-dark，明顯的看得出就是黑底白字樣式。將這兩個名稱複製後，替換第一個 div class 屬性的 bg-light。

重新整理後，黑底白字的樣式已經出現:

![035](assets/bootstrap/035.div-bg-text.jpeg)

另外一種查找方法，可以使用官網中的 Docs 頁籤，搜尋 bg-dark 文字

![036-1](assets/bootstrap/036-1.bootstrap-docs.jpeg)

有更多的樣式可以替換

![036-2](assets/bootstrap/036-2.bootstrap-docs.jpeg)

例如: 使用這個 **bg-info、text-dark** 右下角的 div 區塊就變成藍底黑字

![036-3](assets/bootstrap/036-3.bootstrap-docs.jpeg)

<br>

**調整二 : 下方的 div 區塊寬度**

當前範例中，觀察單一的 div 區塊的 class 樣式是兩個 col-md-6

```html
<div class="col-md-6">
    <div class="h-100 p-5 text-white bg-dark rounded-3">
      <h2>Change the background</h2>
      <p>Swap the background-color utility and add a `.text-*` color utility to mix up the jumbotron look. Then, mix and match with additional component themes and more.</p>
      <button class="btn btn-outline-light" type="button">Example button</button>
    </div>
  </div>
  <div class="col-md-6">
    <div class="h-100 p-5 bg-light border rounded-3">
      <h2>Add borders</h2>
      <p>Or, keep it light and add a border for some added definition to the boundaries of your content. Be sure to look under the hood at the source HTML here as we've adjusted the alignment and sizing of both column's content for equal-height.</p>
      <button class="btn btn-outline-secondary" type="button">Example button</button>
    </div>
  </div>
</div>
```

![037-1](assets/bootstrap/037-1.col-md-6.jpeg)

	數字是 6 + 6 = 12

<br>


複製一個 div，然後將 class 屬性中的 col-md-6 全部改成 col-md-4，呈現的結果維持同一行

![037-2](assets/bootstrap/037-2.col-md-4.jpeg)

	數字是 4 + 4 + 4 也等於 12

<br>

這個是 Bootstrap 中 構建 RWD 排版，重要的**網格系統 (Grid System)**

官網文件(Docs) > Layout > Grid 可以看到相關資訊。

![038-1](assets/bootstrap/038-1.docs-layout-grid.jpeg)
(更多詳細資料 -> Google 搜尋 : Bootstrap 網格系統)

<br>

**網格系統簡單來說是一種排版工具 :**

![038-2](assets/bootstrap/038-2.docs-layout-grid.jpeg)

+ 螢幕尺寸比較大的時候 : 一行最多會以 12 列為單位，超過就會換行
+ 螢幕尺寸比較小的時候 : 畫面會根據螢幕大小適當的調整寬度並且可能換行顯示

```
當移動設備要訪問這個網站時，就能夠瀏覽到合適的佈局。
```

<br>

這邊如果想要查看不同裝置的呈現效果:

1. 按鍵 F12 開啟控制台
2. 點擊控制台左上角第二個 device 圖示
3. 點擊畫面左上角選單，就可以使用不同裝置的螢幕瀏覽網頁。

![039](assets/bootstrap/039.console.jpeg)


<br>

**調整三 : div 區塊中的元件**

官網文件 > Components > 各種元件的樣式

查看 Button 元件，除了能夠調整背景與文字顏色，還可以替換成僅有外框線的按鈕。

![040](assets/bootstrap/040.docs-component.jpeg)

程式碼差異的部分，就是在 class 屬性的樣式。

所以如果要將按鈕的尺寸加大，只要將 btn-lg 的樣式，貼到 button 的 class 屬性。

![041](assets/bootstrap/041.button-style.jpeg)

重新整理，就可以發現按鈕的大小，已經比之前的格式又大了一些。

<br>

**底部文字**

經過前面兩個部分的示範調整，應該已經知道如何透過 Examples 頁面的範例與 Docs 頁面的文件組合使用，完成網頁建置的動作。

最後，調整的下方底部的這一大段資訊就是這兩者的綜合運用。

![042-1](assets/bootstrap/042-1.gamma-ray-studio-about-text.jpeg)

使用的範例是Blog 底部的文字 : 

![042-2](assets/bootstrap/042-2.blog-text.jpeg)

具體的流程 :
1. 在範例中找到文字的 div 標籤複製到專案檔案
2. 接著刪除使用不到的文字內容以及右側的側邊欄連結
3. 將寬度從 col-md-8 調整成 col-md-12

<br>

如此，整體的畫面佈局就已經成形 :

![042-3](assets/bootstrap/042-3.next-task.jpeg)

```
距離最終的成品，還需要許多的試錯與調整。不過有了上面三個部份的示範，應該已經不在毫無頭緒。
剩下的部分隨著參照與調整的數量增加，自然而然就會知道使用哪些樣式可以達成什麼樣的效果，
而這一試錯的過程，也是網頁設計初期學習的絕佳方案。
```



<br>


參考資料
------
+ HTML5 語意標籤 : https://training.pada-x.com/docs/article.jsp?key=html5-semantic-elements







