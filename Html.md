Html 入門指南
======
`Hyper Text Markup Language - 超文本標記語言`

```html
<!DOCTYPE html>
<html>
   <head>
      <title>This is document title</title>
   </head>	
   <body>
      <h1>This is a heading</h1>
      <p>Hello World!</p>
   </body>	
</html>
```
`格式樣態`

+ 超文本 : 將網頁（.HTML）鏈接在一起的方式
+ 標記語言 : 簡單的標記文本檔案，用這些標籤告訴瀏覽器如何顯示

<br>


歷史
------
`作者 : Berners-Lee`
+ HTML : 1991
+ HTML2.0 : 1995
+ HTML4.01 : 1999 `主要版本`
+ HTML5 : 2012 `目前最新`


    開發HTML的目的是定義標題、段落、列表等文檔的結構，以促進研究人員之間的科學信息共享。
    現在，在 HTML 語言中可用的不同標籤的幫助下，HTML 被廣泛用於格式化網頁。


<br>


應用
------
+ 網頁開發
+ 網路導航
+ 響應式介面
+ 離線支援
+ 遊戲開發 `html5`


<br>


主體架構與概念
------
### HTML 標準樣板

**Emmet 操作**
 
    html:5 + Tab

![008](assets/html/008.html-framework.jpeg)

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>

</body>
</html>
```

+ DOCTYPE html : 標註聲明檔案用於 HTML5
+ 標籤(Tag) : `<html>` 起始標籤、`</html>`結束標籤
+ 元素(Element) : `<title>Document</title>`
+ 屬性(Attribute) `後續說明`
  + charset="UTF-8" `編碼`
  + lang="en" `語言`
+ html 結構 : 描述整個網頁
	+ head : 描述網頁的各種配置
		+ 網頁的編碼 : UTF-8
		+ 網頁的標題 : Document
	+ body : 網頁的可視內容，也是開發者最常要編輯的地方

注意: 
```
跟 http 的 header 與 body 不相同，當中的 body 就是 html。
```

<br>

### 示範 : h1 + hr 元件

![009](assets/html/009.h1-hr.jpeg)

```html
<h1>大標題</h1>
<hr>
```

**h1 標籤 : 大標題文字**
`h1 + Tab`
+ 標題 Headings , 編號從 h1 最大 到 h6 最小 

**hr 標籤 : 水平分割線**
`hr + Tab`
+ 縮寫 horizontal rule , 水平分割線

<br>

### 示範 : 六種常見標籤與符號

![010](assets/html/010.six-tag.jpeg)

1. a 標籤
2. button 標籤
3. img 標籤
4. table 標籤
5. ul 標籤
6. br 標籤 與 nbsp 符號

<br>

**1. a 標籤 : 網址連結**
`a + Tab`
```html
<a href="https://www.google.com">Google</a>
<a href="../index.html">Home</a>
<a href="/about/about.html">About</a>
```

+ a Anchor 錨點 -> 訪問 google 搜尋的超連結頁面

<br>

**2. button 標籤 : 按鈕元件**
`button + Tab`
```html
<button onclick="location.href='https://www.google.com'">按鈕</button>
```

+ button 按鈕
	+ onclick 點擊事件
	+ location.href 訪問 Google 網址
+ 搭配 javascript 的程式語言一起使用

<br>

基本概念:

```
網頁畫面中任何可以互動或者動態改變的元素，
背後都有可能是使用 javascript 實作，包含待會介紹的 bootstrap。
```

<br>

**3. img 標籤 : 圖片**
`img + Tab`
```html
<img src="assets/img/gamma-ray-logo2.png" alt="">
```

+ image 圖片 -> assets/img/圖片.png
	+ src : 圖片路徑 

<br>

**4. table 標籤 : 2 x 3 的表格**
`table>tr*2>td*3 + Tab`
```html
<table border="1">
    <tr>
        <td>001</td>
        <td>002</td>
        <td>003</td>
    </tr>
    <tr>
        <td>004</td>
        <td>005</td>
        <td>006</td>
    </tr>
</table>
```

+ table 表格 -> table 、 tr 、 td 標籤
	+ border : 邊框屬性
+ 搭配 javascript 使用 ，呈現大量資料的數據

<br>

**5. ul 標籤 : 項目列表**

`ul>li*5 + Tab`
```html
<ul>
    <li>001</li>
    <li>002</li>
    <li>003</li>
    <li>004</li>
	  <li>005</li>
</ul>
```

+ ul Unordered List 無序列表 -> 搭配 li List 標籤 一起使用

`ol>li*5 + Tab`

```
<ol>
    <li>A</li>
    <li>B</li>
    <li>C</li>
    <li>D</li>
    <li>E</li>
</ol>
```

+ ol Ordered List 有序列表 -> 搭配 li List 標籤 一起使用
 

<br>

Bootstrap:

```
常會被用於向下展開的選單元件
```

<br>

**6. br 標籤 與 nbsp 符號 : 換行與空格**
`br + Tab`
```html
<h2>標題2</h2>
<br>
<br>
<br>
<h3>標題3&nbsp;文字</h3>
```

+ br break space 跳行標籤 -> 間隔三行距離
+ nbsp No-Break Space 不換行空格符號 -> 間隔一個空格

<br>

用途:

```
用在調整畫面的佈局，相比調整 css 的樣式，會比較簡單並且直覺。
```

<br>

### 示範 : 組織畫面結構

![011](assets/html/011.layout.jpeg)

1. div 標籤
2. p 標籤
3. span 標籤

<br>

**1. div 標籤 : 區塊容器**
`div + Tab`
```html
<div></div>
```
+ div division 區塊 -> 存在多組並且區塊內可以增加更多的小區塊

<br>

用途 :
```
網頁設計的佈局排版，大部分會透過這個標籤堆疊而來。(加上 css 樣式 )
```

<br>

**p 標籤 : 段落容器**
`p + Tab`
```html
<p>001</p>
<p>002</p>
<p>003</p>
```
+ p paragraph 段落文字 -> 三段已經換行的文字

<br>

用途:
```
一般的文字呈現，也通常會是使用這個標籤
```

<br>

**span 標籤 : 範圍容器**
`span + Tab`
```
<p>003 <span>&nbsp;範圍</span></p>
```

+ span 範圍 -> 原本應該要換行的段落標籤中，保持同一行並且區分兩者。

<br>

用途:
```
當想要更細緻的切分區塊時，在標籤內部使用 span 範圍標籤。
```

<br>

**有了這三種切分畫面的標籤，就能夠很好的組織畫面結構，並且套用各自的 css 樣式，開發者就可以更精準的描述畫面應該要呈現的樣子。**

<br>

### 示範 : 屬性功能 (Attributes)

+ id , name , class(style)
  + id : 唯一值
  + name : 表單用
  + class : css - style
+ align , width , height
  + align : 位置
  + height : 長
  + width : 寬

```html
<button id="btn_01" name="btn" class="btn-dark">Button</button>

<p align = "left">Left</p> 
<p align = "center">Center</p> 
<p align = "right">Right</p>

<img src="assets/img/gamma-ray-logo2.png" alt="">
<img src="assets/img/gamma-ray-logo2.png" alt="" width="250" height="250"> 
```

<br>

### 示範 : 文字格式 (Formatting)
+ Bold : `b`
+ Italic : `i`
+ Underlined : `u`
+ Strike : `strike`
+ Monospaced : `tt` 
+ Superscript : `sup`
+ Subscript : `sub`
+ Inserted : `ins`
+ Deleted : `del`
+ Larger : `big`
+ Smaller : `small`
+ Emphasized : `em`
+ Marked : `mark`
+ Strong : `strong`
+ Address : `address`

<br>

### 示範 : 套用 css 樣式

<br>

**1. 最簡單方式**

在標籤中新增一個 style 屬性，然後添加 css 的語法

![012-1](assets/html/012-1.html-style.jpeg)

```html
<p style="color: white; background-color: black;">001</p>
```

黑底白字的段落元素:
+ color: white; -> 白色文字
+ background-color: black; -> 黑色背景

```
上述的這種方法，html 的標籤 與 css 的語法 互相交纏在一起。
當 css 語法數量較多或者有大量的 html 標籤套用 style 樣式，程式碼就不容易閱讀。
```

<br>

**2. 集中在 style 標籤**

![012-2](assets/html/012-2.style-tag.jpeg)

這邊的 style 樣式，可以獨立出來全部集中在 head 內的 style 標籤

```html
<head>
	<style>
		<!-- 區分標籤 : 名稱加上左右大括號 -->
		button{
			text-align: center;
			font-size: 30px;
			min-width: 250px;
			min-height: 100px;
			border: 2px solid #001dff;
			-moz-border-radius: 8px;
		}
		.text {
    		color: white;
    		background-color: black;
		}
	</style>
</head>
<body>
	<p class="text">001</p>
</body>

```

+ button : 套用頁面全部的 button 元件
	+ (一般情況，一次性的套用全部標籤是比較少見，更常見的做法會是使用 class 屬性)
+ .text : 套用 classs 屬性為 text 的樣式


<br>

**3. 獨立 css 檔案**

關於 html 標籤 與 css 語法的搭配，進階一點可以將兩者分拆成獨立檔案

![012-3](assets/html/012-3.css-file.jpeg)

index.css :
```css
.text {
    color: white;
    background-color: black;
}
```

使用這種方式，原本 html 的檔案內就不需要使用 style 標籤，只需要透過這個 link 標籤，就可以 將 css 樣式導入。
**除了保持 html 檔案內容的純粹性，此方法也是 Bootstrap 前端框架主要的使用方式。**

<br>

### 示範 : 表單提交 (Form - Submit)

<br>

**Method : GET**

`提交資料查詢系統`

```html
    <form action="submit/target.html" method="GET">
        <div>
            First name: <input type="text" name="first_name" value="OwO/" />
            <br>
            Last name: <input type="text" name="last_name" value="Hi" />
        </div>
        <input type="submit" name="submit" value="Submit" />
    </form>
```

+ input : 輸入框
  + type : text `一般文字`
  + type : submit `提交按鈕`

<br>

**Method : POST**

`提交資料寫入系統`

```html
    <form action="submit/target.html" method="POST">
        <div>
            First name: <input type="text" name="first_name" value="OwO/" />
            <br>
            Last name: <input type="text" name="last_name" value="Hi" />
        </div>
        <br>
        <textarea rows="5" cols="50" name="description">Text</textarea>
        <div>
            <input type="checkbox" name="maths" value="on"> Maths
            <input type="checkbox" name="physics" value="on"> Physics
        </div>
        <div>
            <input type="radio" name="subject" value="maths"> Maths
            <input type="radio" name="subject" value="physics"> Physics
        </div>
        <div>
            <select name="dropdown">
                <option value="Maths" selected>Maths</option>
                <option value="Physics">Physics</option>
            </select>
            <input type="file" name="fileupload" accept="image/*" />
        </div>
        <input type="image" name="img-btn" src="assets/img/gamma-ray-logo2.png" width="50" height="50" />
        <input type="button" name="button" value="Button" />
        <input type="reset" name="reset" value="Reset" />
        <input type="submit" name="submit" value="Submit" />
    </form>
```

+ input : 輸入框
  + type : text `一般文字`
  + type : checkbox `複選`
  + type : radio `單選`
  + type : file `檔案`
  + type : image `按鈕圖片`
  + type : button `按鈕`
  + type : reset `重置按鈕`
  + type : submit `提交按鈕`
+ textarea : 多行文字框
+ select : 下拉式選單

<br>

**POST : API**

+ API : https://reqres.in/

```html
    <form action="https://reqres.in/api/users" method="POST">
        <div>
            First name: <input type="text" name="name" value="Enoxs" />
            <br>
            Last name: <input type="text" name="job" value="Engineer" />
        </div>
        <input type="submit" name="submit" value="Submit" />
    </form>
```

Result :
```
{"name":"Enoxs","job":"Engineer","submit":"Submit","id":"755","createdAt":"2021-07-19T18:54:55.575Z"}
```

<br>

Reference
------
### TutorialsPoint - Html
<https://www.tutorialspoint.com/html/index.htm>

### W3School - Html
<https://www.w3schools.com/html/default.asp>

### Test your front-end against a real API
<https://reqres.in/>



