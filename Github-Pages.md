Github Pages 靜態網頁
------
`GitHub 網頁代管服務，僅能存放靜態網頁`

<br>

### 為什麼選擇使用 github pages?
`三大優點 : 簡單、免費、穩定`

<br>

**簡單部分:**

只要註冊了 GitHub 帳號並且創建特定名稱的專案，就可以立即使用。
剩下的操作的方法，跟一般的 git 操作一樣。

<br>

**免費的部分**

GitHub Pages 的使用限制 :
+ 容量大小 1 GB 以下
+ 流量每個月不超過 100 GB 

```
對於僅僅是要架設個人網站或者部落格，呈現相關資訊的需求相當夠用。
```

<br>

**穩定的部分**

GitHub 是世界上最大的代碼託管平台(開源社群)

並且於 2018 年 被微軟收購，加上大公司的支持，伺服器的穩定性更可以讓人放心一些。

<br>

除了這三個好處以外，如果你剛好也是名軟體工程師，我認為還有另外一個好處 :

**順便證明你會使用 git 版本控制以及理解版本控制的重要性。**

```
這個好處對於剛入行的工程師比較重要，因為我認為在沒有足夠多的作品以及足夠多的技能，可以證明自己的強悍程度時，
一個用 github pages 展示的作品集，可以說明很多事情。
對於履歷內容的呈現或者面試時雙方交流的內容，都能夠起到一個不錯的加分效果。
```

![043](assets/github-pages/043.Tunffli-View.jpeg)
<https://rhzenoxs.github.io/Tunffli-View/>


<br>

### 註冊
+ GitHub 網站 : https://github.com/

![044](assets/github-pages/044.github-account.jpeg)

注意網站的網址會是帳號名稱後方加上 .github.io，雖然還是可以使用 DNS 變更網域，但相對麻煩。
所以在創建帳號時就要想好網址要呈現的樣子。

```
像我這個 Gamma Ray 的官方網站，帳號的名稱就是 Gamma-Ray-Studio
所以他的網址就會變成 gamma-ray-studio.github.io
```

<br>

### 專案
註冊完成後，下一步創建一個帳號名稱加上 .github.io 的專案

![045](assets/github-pages/045.create-project.jpeg)

(以我這個 gamma-ray-studio 的帳號來說，也就是剛才的網址作為專案名稱。)

<br>

### 上傳
`一般 git 專案提交與推送的方法`

如果不太熟悉如何使用 git 工具可以參照下列的這幾個 git 筆記的影片，有更深入說明 :

+ [【git 基礎教程 #1】什麼是 git ? | Sourcetree 介紹 與 入門基礎操作教學](https://youtu.be/MeQWrGrTfOw)
+ [【git 基礎教程 #2】如何開始多人協作 ? | 5 項 git 操作教學](https://youtu.be/5ff9ujdK0IY)
+ [【git 基礎教程 #3】如何追蹤歷史紀錄？ | 如何重置提交訊息? | 版本追蹤進階操作](https://youtu.be/vMCkYWM0k3Y)
+ [【什麼是 git flow ?】 5 項分支全詳解 | Sourcetree 實戰演練](https://youtu.be/B1ZANddOHyM)
+ [【git 提交訊息格式】如何描述提交資訊 ? |  9 種標頭型態 | 結構說明](https://youtu.be/5DhWqmDP-3U)

<br>

**這邊直接以 Sourcetree 工具進行示範**

+ sourcetree : <https://www.sourcetreeapp.com/>

點擊 New > Clone from URL > 填入 git 網址 > 選擇本地端的路徑 > clone 

![046](assets/github-pages/046.clone-project.jpeg)

<br>

克隆完成後，是一個空的專案，這時候就可以將之前已經完成的網頁檔案，放入到專案內，然後提交、推送

![047](assets/github-pages/047.copy-commit-push.jpeg)

<br>

完成後重新整理，可以看見專案的內容已經更新 

![048](assets/github-pages/048.refresh-project.jpeg)

<br>

稍微等待一下，訪問 github pages 網址，就可以看見個人網站已經完成架設。

![049](assets/github-pages/049.gamma-ray-studio.jpeg)

+ Github Pages - Gamma Ray Studio : https://gamma-ray-studio.github.io/zh-cht/index.html




