網路基礎概念
======
`技術名詞解釋`

目錄
------
+ 網路的本質 ?
+ 什麼是 HTTP ? 
+ 什麼是 TCP/IP ?
+ 什麼是 API ?
+ 常見資料格式
+ 網路交換資料格式 : SOAP
+ 網路交換資料格式 : 其他


<br>

網路的本質 ?
-----
`溝通 : 交換資料`

<br>

### 階段一
`傳紙條 : 一對一`

+ 來源與目標 `基本訊息守則`
+ 訊息傳遞三次 `避免訊息丟失`

<br>

### 階段二
`小生意 : 一對多`

+ 格式不統一
    + 1. 2. 3.
    + ㄧ、二、三
    + one , two , three
+ 動作不統一
    + 紙條
    + 口頭
+ 特殊需求
  + 時間
  + 加密
  + 格式
+ 回覆格式
  + 訊息太多
  + 休息中
  + 廠商沒貨

**解決方案**

+ 格式不統一 -> 統一格式
+ 動作不統一 -> 動作規範
    + Get : 取得資訊
    + Post : 提交交易
    + Put : 修改
    + Delete : 刪除
+ 特殊需求 -> 內容規範
    + Header `放特殊需求，可以無限擴充`
    + Body `正規格式化的內容需求`
+ 回覆格式 -> 代碼對照
    + 200 : success
    + 400 : can not read
    + 404 : can not find

<br>

### 階段三
`大生意 : 多對多`

+ 切分業務 -> 一對一服務
+ 提供其他服務 -> 統一服務代碼
    + 80 - 便當
    + 3000 - 飲料
    + 4000 - 點心
+ 優化處理流程
  + 簡化格式 -> 不同的服務內容不同格式
  + 加快速度 -> 即時訊息，不用三次確認
  + 地址地標 -> 有明顯地標的不寫地址也可以

<br>

### 結論

     一堆人在溝通最重要的就是要有標準

**為什麼 ?**

    有了標準，才容易規模化。

**網路名詞 : Http 協定**

    協定 = 標準

<br>

什麼是 Http ?
------
+ HyperText Transfer Protocol
+ 超文本傳輸協定 (標準)
+ 全球資訊網通訊基礎

<br>

### Http 傳輸流程
+ client -- request --> server
+ client <- response -- server
+ browser : html -> render

<br>

### DNS 伺服器
+ Domain Name System
+ client -- www.google.com -> dns server
+ client <- 10.1.1.119 -- dns server

<br>

### HOST 主機
+ /etc/host
+ domain name -- mapping -> ip address

**尋找主機**
```
    $ nslookup google.com
```

<br>

### 瀏覽器
`客戶端的應用程式`

    使用者不用看懂協定，但工程師要略懂才能寫 Code

<br>

### HTTP 傳輸方法
+ GET
    + GET 方法請求展示指定資源。使用 GET 的請求只應用於取得資料。
+ HEAD
    + HEAD 方法請求與 GET 方法相同的回應，但它沒有回應主體（response body）。
+ POST
    + POST 方法用於提交指定資源的實體，通常會改變伺服器的狀態或副作用（side effect）。
+ PUT
    + PUT 方法會取代指定資源所酬載請求（request payload）的所有表現。
+ DELETE
    + DELETE 方法會刪除指定資源.
+ CONNECT
    + CONNECT 方法會和指定資源標明的伺服器之間，建立隧道（tunnel）。
+ OPTIONS
    + OPTIONS 方法描述指定資源的溝通方法（communication option）。
+ TRACE
    + TRACE 方法會與指定資源標明的伺服器之間，執行迴路返回測試（loop-back test）。
+ PATCH
    + PATCH 方法套用指定資源的部份修改。

<br>

### HTTP 狀態碼
+ https://zh.wikipedia.org/wiki/HTTP%E7%8A%B6%E6%80%81%E7%A0%81A

+ 301 重新導向 (永久)
  + GET : a.com
  + Status Code : 301
      + Location : b.com
  + GET : a.com -> b.com
+ 302 重新導向（暫時）
+ 4xx 用戶端錯誤
+ 5xx 伺服端錯誤

**分類**
+ 1xx. - Hold on
+ 2xx. - Here you go
+ 3xx. - Go away
+ 4xx. - You fucked up
+ 5xx. - I fucked up

<br>

### 結論
+ HTTP 協定 就是網際網路交換資料的標準
+ 瀏覽器是一種應用程式，用來看懂網路協定的工具
+ Header 是給瀏覽器看，Body 是給真實人類看

<br>

什麼是 TCP/IP ?
------
### 網路的層級
+ OSI 七層 (課本）
    + 應用層 - Application
    + 表現層 - Presentation
    + 會談層 - Session
    + 傳送層 - Transport
    + 網路層 - Network
    + 連結層 - Data Link 
    + 實體層 - Physical
+ TCP/IP 四層 (主流)
    + 應用層 - Application / Presentation / Session
        + Application Layer
        + Http / FTP / SMTP / POP3 / NFS / SSH
    + 傳輸層 - Transport
        + Transport Layer
        + TCP / UDP
    + 網路層 - Network
        + Internet Layer
        + IP / ICMP
    + 網路接口層 - Data Link / Physical
        + Network Access (link) Layer
        + LAN / WAN / ARP

<br>

### IP 地址
`Internet Protocol - 網路協議地址`

+ IPv4 - ex : 192.168.1.1
+ IPv6 - ex : 2001:0db8:02de:0000:0000:0000:0000:0e13

<br>

### 虛擬 IP 、浮動 IP 與 固定 IP
+ 固定 IP
    + 伺服器
    + 公司企業
+ 浮動 IP
    + 一般使用者
    + 安全需求
+ 虛擬 IP
    + 數據機： 固定 或 浮動 IP
    + 內網：虛擬 IP

<br>

### Port 的作用
`連接埠 / 端口`

+ http : 80
+ https : 443
+ ftp : 21
+ test : 8080 , 3000 , 4000 , 4001

<br>

### TCP 與 UDP
+ 傳輸層的兩個協議
+ TCP
    + 可靠
+ UDP
    + 快速
    + 應用 : 視訊

<br>

### 溝通 : 三次握手
`Three way handshake`

1. client -- SYN -> Server
2. client <- SYN-ACK -- Server
3. client -- ACK -> Server

<br>

### 結論
+ HTTP : 紙條上的內容，包含 Header 與 Body
+ TCP / UDP 
    + 傳紙條時的三次確認
    + 不確認，一直傳紙條
+ IP : 寄紙條，收件者與寄件人
+ 實體層 : 幫忙寄信的郵差


<br>

什麼是 API ?
------
`應用程式介面(Application Programming Interface , API)`

+ 使用 API
    + 資料來自於資料庫
    + 不需要資料庫權限
+ 提供 API
    + 作業系統(OS)
        + 網路狀況
        + 讀取檔案
    + Facebook
        + 好友資料
    + 網站上的資訊
    + 新增資料

```
透過 API，可以讓雙方交換資料
```

<br>

### API 與 Web API 
+ Web API => Http Api
    + Facebook API
    + Twitter API
+ SDK - Software Development Kit
    + tw.getTimeLine()

<br>

常見資料格式
------
`純文字與自定義格式`

+ XML
+ JSON

<br>

### XML
`Extensible Markup Language`

```xml
<?xml version="1.0" endcoding="UTF-8"?>
<note>
    <to></to>
    <from></from>
    <head></head>
    <body></body>
</note>
```

<br>

### JSON
`JavaScript Object Notation`

```json
{
  "data" : {
    "id" : "1" ,
    "name" : "JavaProjSE"
  }
}
```

<br>

網路交換資料格式 : SOAP
------
### SOAP 簡介
`Simple Object Access Protocol`

+ XML 交換資料
+ 有興趣再研究，老專案很常見

<br>

網路交換資料格式 : 其他
------
### 什麼叫做「其他的」API ?
`不知道怎麼歸類`

<br>

### RESTful 到底是什麼 ?
+ 不是一個協定
+ 只是一種風格（建議）

Action | Descript | Format 
------ | ------   | ------
POST   | 新增使用者 | /users
DELETE | 刪除使用者 | /users/:id
GET    | 查詢使用者 | /users/:id
GET    | 使用者列表 | /users
PATCH  | 更改使用者 | /users/:id








